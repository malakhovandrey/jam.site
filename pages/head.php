<!-- Мета -->
<meta http-equiv="content-type" content="text/html; charset=utf-8" />

<!-- Заголовок страницы -->
<title><?php echo $page->title; ?></title>

<!-- Стили -->
<link rel="stylesheet" type="text/css" href="../site/templates/styles/main.css" />
	<!-- bxSlider CSS file -->
	<link href="../site/templates/styles/slider/jquery.bxslider.css" rel="stylesheet" />

<!-- Шрифты -->
<link href='http://fonts.googleapis.com/css?family=PT+Sans:400,700,400italic,700italic&subset=latin,cyrillic' rel='stylesheet' type='text/css'>

<!-- Библиотеки -->
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>

<!-- Скрипты -->
<script src="../site/templates/scripts/main.js"></script>
	<!-- bxSlider Javascript file -->
	<script src="../site/templates/scripts/slider/jquery.bxslider.min.js"></script>
	<script text="javascript">
		$(document).ready(function(){
			$('.bxslider').bxSlider();
		});
	</script>