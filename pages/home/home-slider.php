<div id="home-slider">
	<ul class="bxslider">
		<li>
			<img src="../site/templates/images/home/slider/slide-1.jpg" />
			<div class="home-slider slider-text-div-1">
				<font class="home-slider slider-text-white bold shadow">Стильная татуировка на 2 недели!</font>
				<br>
				<font class="home-slider slider-text-small-white normal shadow">Создавай шедевры дома</font>
			</div>
			<div class="home-slider slider-button-div-1"><a href="/catalog" class="home-slide slider-button-text">Смотреть каталог</a></div>
		</li>
		<li>
			<img src="../site/templates/images/home/slider/slide-2.jpg" />
		</li>
		<li>
			<img src="../site/templates/images/home/slider/slide-3.jpg" />
		</li>
	</ul>
</div>