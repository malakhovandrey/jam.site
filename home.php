<!DOCTYPE html>
<html lang="en">
	<head>
		<?php include("pages/head.php"); ?>
	</head>
	<body>
		<div id="wrapper">
			<?php include("pages/top-menu/top-menu.php"); ?>
			<?php include("pages/home/home-slider.php"); ?>
			<?php include("pages/home/home-new.php"); ?>
			<?php include("pages/home/home-howtouse.php"); ?>
			<?php include("pages/home/home-qoutes.php"); ?>
			<?php include("pages/home/home-subscription.php"); ?>
			<?php include("pages/home/home-footer.php"); ?>
		</div>
	</body>
</html>