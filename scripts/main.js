$(document).ready(function() {
	
// top-menu / Верхнее меню
	var element = $(".top-menu");
	$(window).scroll(function() {
		var scroll = $(window).scrollTop();
		if (scroll >= 0 && scroll < 17) {
            $(".top-menu").css({"background": "linear-gradient(to right,  rgba(255,255,255,0) 0%,rgba(255,255,255,0) 30%,rgba(255,255,255,0.65) 45%,rgba(255,255,255,0.65) 100%)"});
        }
		else if (scroll >= 18) {
            $(".top-menu").css({"background": "rgba(255, 255, 255, 0.65)"});
        }
	});
	

// top-menu top-logo / Логотип на главной
	var element = $(".top-logo");
	$(window).scroll(function() {
		var scroll = $(window).scrollTop();
		if (scroll >= 0 && scroll < 17) {
            $(".top-logo").css({"width": "250px"});
            $(".top-logo").css({"height": "250px"});
        }
		else if (scroll >= 18) {
            $(".top-logo").css({"width": "50px"});
            $(".top-logo").css({"height": "50px"});
        }
	});
	
});